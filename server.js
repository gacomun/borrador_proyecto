require('dotenv').config();
const express = require('express');//inclusión del modulo express
const app=express();// Inicializa el framework

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 // This will be needed.
 res.set("Access-Control-Allow-Headers", "Content-Type");

 next();
}

const userController =require("./controllers/UserController")
const authController =require("./controllers/AuthController")
const authControllerV2 =require("./controllers/AuthControllerV2")
const accountControllerV2 =require("./controllers/AccountControllerV2")

const port = process.env.PORT || 3000; //por convecion las variables van en mayusculas
app.use(express.json());
app.use(enableCORS);
app.listen(port);
console.log("API escuchando en el puerto " + port);

app.get("/apitechu/v1/hello",
  function(req,res) {
      console.log("---------------\nGET /apitechu/v1/hello");

      res.send({"msg":"Hola desde API TechU"})
  }
)

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req,res) {
    console.log("---------------\nPOST /apitechu/v1/monstruo/:p1/:p2");
    console.log("Parametros");
    console.log(req.params);
    console.log("Query String");
    console.log(req.query);
    console.log("Headers");
    console.log(req.headers);
    console.log("Body");
    console.log(req.body);

      res.send({"msg":"Hola monstruo"})
  }
)

app.get("/apitechu/v1/users",userController.getUsersV1);
app.get("/apitechu/v2/users",userController.getUsersV2);
app.get("/apitechu/v2/users/:id",userController.getUserbyIdV2);
app.post("/apitechu/v1/users",userController.createUserV1);
app.post("/apitechu/v2/users",userController.createUserV2);
app.delete("/apitechu/v1/users/:id",userController.deleteUserV1);

app.post("/apitechu/v1/login",authController.postLoginV1);
app.post("/apitechu/v1/logout/:id",authController.postLogoutV1);
app.post("/apitechu/v2/login",authControllerV2.postLoginV2);
app.post("/apitechu/v2/logout/:id",authControllerV2.postLogoutV2);

app.get("/apitechu/v2/accounts",accountControllerV2.listAccountsV2);
