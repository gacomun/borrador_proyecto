const requestJson=require("request-json");

const baseMLABUrl="https://api.mlab.com/api/1/databases/apitechujco12ed/collections/";
const mLabAPIKey = "apiKey="+ process.env.MLAB_API_KEY;

function listAccountsV2(req,res) {
    console.log("---------------\GET /apitechu/v1/accounts");
    console.log(req.query);
    var mensaje={"msg":"ok"};
    if(req.query.id_user)
    {
      var id = Number.parseInt(req.query.id_user);
      if(Number.isNaN(id)){
          mensaje={"msg":"identificador no es un número"};
          res.status(400);
          res.send(mensaje);
      }
      else{
        var q="q="+JSON.stringify({'id_user':id})+"&";
        var httpClient=requestJson.createClient(baseMLABUrl);
        console.log("Client created");

          httpClient.get("accounts?"+q+mLabAPIKey,
            function(err,resMlab, body){
              if(err){
                mensaje = {
                  "msg": "Error obteniendo cuenta"
                }
                res.status(500);
              }else {
                mensaje= body;
              }
              res.send(mensaje);

            }

          );
      }
    }
    else{
      mensaje={"msg":"implementación no soportada"};
      res.status(400);
      res.send(mensaje);

    }
}

module.exports.listAccountsV2 = listAccountsV2;
