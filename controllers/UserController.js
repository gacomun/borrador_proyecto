const io =require("../utils/io");
const crypt =require("../utils/crypt");
const requestJson=require("request-json");

const baseMLABUrl="https://api.mlab.com/api/1/databases/apitechujco12ed/collections/";
const mLabAPIKey = "apiKey="+ process.env.MLAB_API_KEY;

function getUsersV1(req,res) {
  console.log("---------------\nGET /apitechu/v1/users");
  console.log("Query String");
  console.log(req.query);
  var dev={}

  var users = require("../users.json");
  var fcount=req.query.$count;
  if(fcount=='true'){
    dev.count=users.length
  }

  var ftop=req.query.$top;
  dev.users=(ftop)?users.slice(0,ftop):users
  //if(ftop!=null){
  //    dev.users=users.slice(0,ftop)
  //}
  //else{
  //  dev.users=users
  //      }

  //res.sendFile("users.json",{root: __dirname});
  res.send(dev);

}

function getUsersV2(req,res) {
  console.log("---------------\nGET /apitechu/v2/users");
  var httpClient=requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.get("user?"+mLabAPIKey,
    function(err,resMlab, body){
      var response = ! err ? body :{
        "msg": "Error obteniendo usuarios"
      }
      res.send(response);

    }

  );

}

function getUserbyIdV2(req,res) {
  console.log("---------------\nGET /apitechu/v2/users/"+req.params.id);
  var httpClient=requestJson.createClient(baseMLABUrl);
  console.log("Client created");
var id = Number.parseInt(req.params.id);
var q="q="+JSON.stringify({'_id':id})+"&";

  httpClient.get("user?"+q+mLabAPIKey,
    function(err,resMlab, body){
      if(err){
        var response = {
          "msg": "Error obteniendo usuario"
        }
        res.status(500);
      }else if (body.length > 0) {
        var response= body[0];
      }else {
        var response = {
          "msg": "Usuario no encontrado"
        }
        res.status(404);
      }
      res.send(response);

    }

  );

}

function createUserV1(req,res) {
  console.log("---------------\nPOST /apitechu/v1/users");

  // console.log(req.body)
  console.log(req.body.id)
  console.log(req.body.first_name)
  console.log(req.body.last_name)
  console.log(req.body.email)
  var newUser={
    "id" :req.body.id,
    "first_name" :req.body.first_name,
    "last_name" :req.body.last_name,
    "email" :req.body.email
  }
  console.log(newUser);
  var users = require("../users.json");
  users.push(newUser)
  io.writeUserDataToFile(users);
  console.log("Usuario añadido al array");
  res.send({"msg":"Usuario creado correctamente"});



}

function createUserV2(req,res) {
  console.log("---------------\nPOST /apitechu/v2/users");

  // console.log(req.body)
  var newUser={
    "_id" : ""+req.body.id,
    "id" :req.body.id,
    "first_name" :req.body.first_name,
    "last_name" :req.body.last_name,
    "email" :req.body.email,
    "password" :crypt.hash(req.body.password)
  }
  console.log(newUser);
  var httpClient=requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.post("user?"+mLabAPIKey,newUser,
  function(err,resMlab, body){
    if(err){
      var response = {
        "msg": "Error alta usuario"
      }
      res.status(500);
    }else {
      var response = {
        "msg": "Usuario Creado"
      }
      res.status(201);
    }
    res.send(response);

  }

  );




}
function deleteUserV1(req,res) {
  console.log("---------------\nDELETE /apitechu/v1/users/"+req.params.id);

  var users = require("../users.json");
  var indice=-1;

  // metodo 1
  //    for(var i=0;i<users.length;i++){
  //      if(users[i].id== req.params.id){
  //        indice=i;
  //        break;
  //      }
  //    }

  // metodo 2
  //    for(user in users){
  //            if(users[user].id== req.params.id){
  //              indice=user;
  //              break;
  //            }
  //    }

  // metodo 3
  // users.forEach(function(user,index) {
  //   if(user.id==req.params.id){
  //     indice=index;
  //     console.log("indice="+indice);
  //   }
  // });
  // metodo 4
  //   var indiceTempo=-1;
  //   for (user of users){
  //     indiceTempo++;
  //     if(user.id==req.params.id){
  //       indice=indiceTempo;
  //       console.log("indice="+indice);
  //       break;
  //     }
  // };
  // metodo 4.1
  //   for (var [index,user] of users.entries()){
  //     if(user.id==req.params.id){
  //       indice=index;
  //       console.log("indice="+indice);
  //       break;
  //     }
  // };
  //metodo 5
  var indice=users.findIndex(function(element) {
    return element.id == req.params.id;
  })
  console.log("indice="+indice);
  if(indice!=-1){
    users.splice(indice,1)
    io.writeUserDataToFile(users)
    res.send({"msg":"Usuario borrado correctamente"});

  } else{
    res.statusCode = 400;
    res.send({"msg":"Usuario no encontrado"});

  }




}


module.exports.getUsersV1 = getUsersV1;
module.exports.createUserV1 = createUserV1;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserbyIdV2 = getUserbyIdV2;
module.exports.createUserV2 = createUserV2;
