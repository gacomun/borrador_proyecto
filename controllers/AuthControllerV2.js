  const requestJson=require("request-json");
  const crypt =require("../utils/crypt");

  const baseMLABUrl="https://api.mlab.com/api/1/databases/apitechujco12ed/collections/";
  const mLabAPIKey = "apiKey="+ process.env.MLAB_API_KEY;

  function postLoginV2(req,res) {
    console.log("---------------\nPOST /apitechu/v2/login");
    var httpClient=requestJson.createClient(baseMLABUrl);
    console.log("Client created");

    // es una vulneravilidad de seguridad
    //var q="q={'email':'"+req.body.email+"'}&";
    var q="q="+JSON.stringify({'email':req.body.email})+"&";

    httpClient.get("user?"+ q + mLabAPIKey,
    function(err,resMlab, body){
      if(err){
        var response = { "mensaje" : "Login incorrecto" }
        res.status(500);
        res.send(response);
      }else if (body.length <= 0) {
        var response = { "mensaje" : "Login incorrecto" }
        res.status(400);
        res.send(response);
      }else {
        var iguales=crypt.checkPassword(req.body.password,body[0].password);
        if(iguales === true){
          var query="q="+JSON.stringify({'_id':body[0]._id})+"&";
          var putBody = '{"$set":{"logged":true}}';
          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPut,resMlabPut, bodyPut){
              if(errPut){
                var response = { "mensaje" : "Login incorrecto" }
                res.status(500);
                res.send(response);
              }else{
                var response = { "mensaje" : "Login correcto", "idUsuario" : body[0].id };
                res.status(200);
                res.send(response);
              }
            }
        );

      }else {
        var response = { "mensaje" : "Login incorrecto" }
        res.status(400);
        res.send(response);

      }
    }

  }

  );


  }

  function postLogoutV2(req,res) {
    console.log("---------------\nPOST /apitechu/v2/logout/"+req.params.id);
    var httpClient=requestJson.createClient(baseMLABUrl);
    console.log("Client created");

     var id = Number.parseInt(req.params.id);
    var q="q="+JSON.stringify({'$and':[{'id':id},{'logged':true}]})+"&";
    console.log("user?"+ q + mLabAPIKey);

    httpClient.get("user?"+ q + mLabAPIKey,
      function(err,resMlab, body){
        if(err){
          var response = { "mensaje" : "Logout incorrecto" }
          res.status(500);
          res.send(response);
        }else if (body.length <= 0) {
          var response = { "mensaje" : "Logout incorrecto" }
          res.status(400);
          res.send(response);
        }else {
          var query="q="+JSON.stringify({'_id':body[0]._id})+"&";
          var putBody = '{"$unset":{"logged":""}}';
          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPut,resMlabPut, bodyPut){
              if(errPut){
                var response = { "mensaje" : "Logout incorrecto" }
                res.status(400);
                res.send(response);
              }else{
                var response = { "mensaje" : "Logout correcto", "idUsuario" : id };
                res.status(200);
                res.send(response);
              }
            }
        );

        }
      }
    );
  }


  module.exports.postLoginV2 = postLoginV2;
  module.exports.postLogoutV2 = postLogoutV2;
