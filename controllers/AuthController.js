const io =require("../utils/io")


function postLoginV1(req,res) {
  console.log("---------------\nPOST /apitechu/v1/login");
  // console.log(req.body);
  var users = require("../users.json");
  var indice=users.findIndex(function(element) {
    return element.email == req.body.email && element.password == req.body.password ;
  })
  console.log("indice="+indice);
  if(indice!=-1){
    users[indice].logged=true;
    io.writeUserDataToFile(users)
    res.send( { "mensaje" : "Login correcto", "idUsuario" : users[indice].id });

  } else{
    res.statusCode = 400;
    res.send({ "mensaje" : "Login incorrecto" });

  }
}

function postLogoutV1(req,res) {
  console.log("---------------\nPOST /apitechu/v1/logout/"+req.params.id);
  console.log(req.body);
  var users = require("../users.json");
  var id = Number.parseInt(req.params.id);
  var indice=users.findIndex(function(element) {
    // en condicion element.logged solo mira que este definido
    // si se hace element.logged== true hace un casteo y no mira valor y tipo
    return element.id == id && element.logged===true ;
  })
  console.log("indice="+indice);
  if(indice!=-1){
    delete users[indice].logged
    io.writeUserDataToFile(users)
    res.send( { "mensaje" : "Logout correcto", "idUsuario" : users[indice].id });

  } else{
    res.statusCode = 400;
    res.send({ "mensaje" : "Logout incorrecto" });

  }

}


module.exports.postLoginV1 = postLoginV1;
module.exports.postLogoutV1 = postLogoutV1;
