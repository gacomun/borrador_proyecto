# Imagen raiz
FROM node

# Carpeta raiz
WORKDIR /apitechu

# Copia de archivos de carpeta local apitechu
ADD . /apitechu

# Instalación de las dependencias (se ejecuta mientras se esta construyendo)
RUN npm install --only=prod

# Puerto de trabajo
EXPOSE 3000

#Comando de inicialización
CMD ["node", "server.js"]
